import java.util.Arrays;
import java.util.Collections;

public class Sheep {

   enum Animal {sheep, goat};

   public static void main(String[] param) {
      Animal[] loomad = {Animal.sheep,Animal.goat,Animal.sheep,Animal.goat,Animal.goat,Animal.sheep,Animal.sheep,Animal.sheep,Animal.sheep};
      reorder(loomad);
   }

   public static void reorder(Animal[] animals) {

      /**
       * Solution: Count the numbers of goats and create a new array based on the result.
       */
      int count = 0;
      for (Animal a : animals) { if (a == Animal.goat) count++; }
      for (int i = 0; i < count; i++) animals[i] = Animal.goat;
      for (int i = count; i < animals.length; i++) animals[i] = Animal.sheep;

      /**
       * Solution no 7: Count the value "goat" in the array and swap the value in the position
       * i with the value of the counted position each time a "goat" value is found in the array.
       */
      int c = 0;
      for (int i = 0; i < animals.length; i++) {
         if (animals[i]==Animal.goat) {
            animals[i]=animals[c];
            animals[c]=Animal.goat;
            c++;
         }
      }

      /**
       * Solution no 6: Sort by bringing a value from the end of the list to the position i
       * every time a "sheep" value is found in the array
       */
      int r = animals.length-1;
      for (int i = 0; i <= r; i++) {
         if (animals[i]==Animal.sheep) {
            animals[i]=animals[r];
            animals[r]=Animal.sheep;
            r--;
            i--;
         }
      }

      /**
       * Solution no 5: Sort using a boolean variable
       */
      int cv = 0;
      boolean t = false;
      for (int i = 0; i < animals.length; i++) {
         if (animals[i]==Animal.goat && !t) {
            cv++;
         } else if (animals[i]==Animal.goat && t) {
            animals[c]=Animal.goat;
            animals[i]=Animal.sheep;
            cv++;
         } else if (animals[i]==Animal.sheep && !t) {
            t = true;
         }
      }

      /**
       * Solution no 4: Go through the array from back to the front switching elements.
       * As an extra step, if both, the value from the beginning of the array and from
       * the end of array are "goat", the next value from the beginning is moved to the
       * position in the back.
       */
      int l = 0;

      for (int i = animals.length-1; i > l; i--) {
         if (animals[i]==Animal.sheep && animals[l]==Animal.goat) {
            l++;
         } else if (animals[i]==Animal.goat && animals[l]==Animal.goat) {
            animals[i]=animals[l+1];
            animals[l+1]=Animal.goat;
            i++;
            l=l+2;
         } else if (animals[i]==Animal.goat && animals[l]==Animal.sheep) {
            animals[i]=Animal.sheep;
            animals[l]=Animal.goat;
            l++;
         }
      }

      /**
       * Solution no 3: Sort using Collections framework
       */
      Collections.sort(Arrays.asList(animals));
      Collections.reverse(Arrays.asList(animals));

      /**
       * Solution no 2: Sort by comparing the length of the strings in the array.
       */
      for (int i = 0; i < animals.length; i++) {
         for (int j = i; j > 0; j--) {
            while (j > 0 && animals[j - 1].toString().length() > animals[j].toString().length()) {
               animals[j - 1] = Animal.goat;
               animals[j] = Animal.sheep;
            }
         }
      }

      /**
       * Solution no 1: Count the numbers of goats and create a new array based on the result
       */
      int gc = 0;
      for (int i = 0; i < animals.length; i++) {
         if (animals[i] == Animal.goat) {
            gc++;
         }
      }
      for (int i = 0; i < gc; i++) {
         animals[i] = Animal.goat;
      }
      for (int i = gc; i < animals.length; i++) {
         animals[i] = Animal.sheep;
      }

   }
}
